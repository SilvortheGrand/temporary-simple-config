# Temporary Simple Config
This is a simple config suite for TF2, allowing for huge customizability with scripting with a little elbow grease.

## Installation
To install the config, download the files. Then simply drag & drop the files into the `tf/cfg/` folder. You can find it by right-clicking TF2 in your Steam library, and then clicking `Manage > Browse local files`. Lastly, you need to open the `autoexec.cfg` file and add `exec TSC_init.cfg` to it on a new line.
If you'd like to see the config start up or the config is broken for you, I suggest adding the `-console` launch option to TF2, which you can add by right clicking on TF2 in the Steam Library, clicking `Properties`, clicking the `General` tab and scrolling down.

## Updating
If you'd like to update to a newer version of this config, you can run the `TSC_UpdateToLatest.ps1` file. You can find it straight in your `tf/cfg` folder.
It fetches all the latest changes from the server and applies them. Be aware that it will overwrite your files doing this. If you don't want that but still would like the latest files, you can download the latest files [here](https://gitlab.com/SilvortheGrand/temporary-simple-config/-/archive/master/temporary-simple-config-master.zip) and choose how to apply them yourself.
If you want to do things manually, I suggest making backups beforehand and using a diff tool (kinda like [this one](https://www.diffchecker.com/)).

## Customizing
There is an (as of now very) barebones [wiki for TSC](https://gitlab.com/SilvortheGrand/temporary-simple-config/-/wikis/home). It contains information on how you can customize your installation of TSC.

## Maintenance Notice
This config will not be maintained by me and bugs are unlikely to be addressed. However, if any do occur and I am able to find time, I will look at the [issues on *GitLab*](https://gitlab.com/SilvortheGrand/temporary-simple-config/-/issues). If you'd like to contribute to development, you may contact me on Discord at `@silvorthegrand`.
