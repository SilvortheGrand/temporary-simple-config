# Updater Script for TSC
Write-Host "=== Temporary Simple Config - Updater"
Write-Host "== This is a simple script created for fetching and installing the latest version of TSC."
Write-Host "== [INFO] The updater iself is NOT updated when running it. To update it to the latest"
Write-Host "== [INFO] version (recommended), please go to"
Write-Host "== [INFO] https://gitlab.com/SilvortheGrand/temporary-simple-config/-/archive/master/"
Write-Host "== [INFO] and download it separately. IMPORTANT: IT HAS TO BE IN THE CFG FOLDER TO WORK."
Write-Host "== [INFO] This updater might overwrite changes that you made to the TSC config."
Write-Host "= If you continue, the following files will likely be overwritten:"

(Get-ChildItem TSC_*.cfg).BaseName | % {
	Write-Host "= - $_.cfg"
}

Write-Host "== Again, unless you made a backup of your changes, they WILL LIKELY BE LOST."

if ((Read-Host "== Do you wish to proceed? (y/N)") -ne "y") {
	Write-Host -ForegroundColor Green "== Cancelling!"
	return
}

Write-Host "= Update started."
Write-Host "= Downloading files..."

[string] $latestFilesURL = "https://gitlab.com/SilvortheGrand/temporary-simple-config/-/archive/master/temporary-simple-config-master.zip"
[string] $currentLocation = (Get-Location)
[string] $downloadedFilesName =  (-join($currentLocation, "/UpdatedZip.zip"))
try {
	Invoke-WebRequest -uri $latestFilesURL -Method "GET" -Outfile $downloadedFilesName
}
catch {
	Write-Host -ForegroundColor Red "== [ERROR] Download failed. Are you connected to the internet?"
	return
}

Write-Host "= Download successful."
Write-Host "= Unpacking files..."

# Too lazy to add error handling beyond this point

Expand-Archive $downloadedFilesName -DestinationPath $currentLocation -Force

Write-Host "= Files unpacked."
Write-Host "= Overwriting Files..."

Set-Location "temporary-simple-config-master" # Set current directory to unzip directory

Get-ChildItem *.cfg | % {
	Move-Item $_.FullName (-join($currentLocation, "/", $_.BaseName, $_.Extension)) -Force
}

Set-Location $currentLocation # Reset location back to cfg

Write-Host "= Done."
Write-Host "= Cleaning up..."

Remove-Item "temporary-simple-config-master" -Recurse
Remove-Item $downloadedFilesName

Write-Host "= Cleanup complete."

Write-Host -ForegroundColor Green "== Update successful!"
